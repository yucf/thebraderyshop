import './home-component.css'; 
import { useNavigate } from 'react-router-dom';

import React, { useEffect, useState } from 'react';
import { isAuthenticated } from '../../utils/auth-middleware'


const HomeComponent = () => {
  const [isAuthenticatedVar, setIsAuthenticated] = useState(false);
  const navigate = useNavigate();

  function logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    navigate('/login');
  }
  useEffect(() => {
    async function checkAuthentication() {
      const authenticated = await isAuthenticated(); 
      setIsAuthenticated(authenticated);
    }

    checkAuthentication();
  }, []);

  return (
    <div>
      
      <header id="mu-header">
      <nav className="navbar" role="navigation">
        <div className="container">
          
          {!isAuthenticatedVar ? (
            <>
              <a href="/login" className="brand-link">
              Login<span></span>
          </a>
            <a href="/signup" className="brand-link signup-btn">
            Signup<span></span>
          </a>
          </>
        ) : null}
          

          <ul id="top-menu" className="nav-links">
            <li>
              <a href="/" className="nav-link">
                THE BRADERY 0.5
              </a>
            </li>
            <li>
              <a href="#mu-about-us" className="nav-link">
                ABOUT US
              </a>
            </li>
            <li>
              <a href="/products" className="nav-link">
                PRODUCTS
              </a>
            </li>           
            <li>
              <a href="/cart" className="nav-link">
                CART
              </a>
            </li>
            <li>
            {!isAuthenticatedVar ? (
            null
        ) : <button onClick={logout}className="logout brand-link">
        Logout
      </button>}

            </li>
          </ul>
        </div>
      </nav>
    </header>


    <section id="mu-slider">
        
    <div id="carouselExampleCaptions" className="carousel slide">
  <div className="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div className="carousel-inner mu-top-slider">
    <div className="carousel-item mu-top-slider-single active">
    <img className='carousel-img' src={process.env.PUBLIC_URL + "images/home/slider/1.jpg"} alt="img" />
        <div className="mu-top-slider-content">
            <span className="mu-slider-small-title">Welcome</span>
            <h2 className="mu-slider-title">To THE BRADERY</h2>
            <p>Free delivery on orders over €200</p>           
            <a href='/products' className="mu-readmore-btn mu-reservation-btn">Commander</a>
          </div>
    </div>
    <div className="carousel-item mu-top-slider-single">
    <img className='carousel-img' src={process.env.PUBLIC_URL + "images/home/slider/2.jpg"} alt="img" />
        <div className="mu-top-slider-content">
            <span className="mu-slider-small-title">The Elegant</span>
            <h2 className="mu-slider-title">All responsible</h2>
            <p>The Bradery defends the idea of more responsible and affordable fashion, by (re)creating desire for sleepy pieces.</p>           
            <a href='/products' className="mu-readmore-btn mu-reservation-btn">Commander</a>
          </div>
    </div>
    <div className="carousel-item mu-top-slider-single">
    <img className='carousel-img' src={process.env.PUBLIC_URL + "images/home/slider/3.jpg"} alt="img" />
        <div className="mu-top-slider-content">
            <span className="mu-slider-small-title">Offer</span>
            <h2 className="mu-slider-title">GIVE 20€ GET 20€</h2>
            <p>Offer €20 to a friend on their first order, and receive €20 in vouchers.</p>           
            <a href='/products' className="mu-readmore-btn mu-reservation-btn">Commander</a>
          </div>
    </div>
  </div>
  <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Previous</span>
  </button>
  <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Next</span>
  </button>
</div>
  </section>

      {/* About Us Section */}
  <section id="mu-about-us">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="mu-about-us-area">

            <div className="mu-title">
              <span className="mu-subtitle">Discover</span>
              <h2>ABOUT US</h2>
            </div>

            <div className="about-sec row">
              <div className="col col-md-6">
               <div className="mu-about-us-left">     
               <img src={process.env.PUBLIC_URL + "images/home/slider/4.jpg"} alt="img" />
                </div>
              </div>
              <div className="col col-md-6">
                 <div className="mu-about-us-right">
                 <p>The Bradery offers ephemeral sales of premium brands on Instagram for individuals aged 20-35.</p>
<p>Our mission is to enhance existing items by giving new life to dormant pieces from premium brands. We create high-quality, Insta-friendly content to showcase selected products from the past collections of our partner brands.</p>
<p>For our community, we make the inaccessible accessible by valuing what already exists; through a conscious shopping experience.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

    </div>
  );
};

export default HomeComponent;
