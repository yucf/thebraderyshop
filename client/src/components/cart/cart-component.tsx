import React, { useState, useEffect } from 'react';
import './cart-component.css';
import { Product } from '../../interfaces/Product';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

const CartComponent = () => {
  const [cartItems, setCartItems] = useState<Product[]>([]);
  const [total, setTotal] = useState(0);
  const navigate = useNavigate();


  
  const deleteMeal = async (mealId: any) =>{
    const userId = localStorage.getItem('user');
    let res = await fetch(`http://localhost:4000/cart/delete/${mealId}/${userId}`, {
      method: "PUT",
      headers: {'Content-Type': 'application/json'},
    });
    if (res.status === 200) {
      window.location.reload();
    }
  }
  const saveCart = async (cartItems: any[]) =>{
    const body = {cartItems};
    const userId = localStorage.getItem('user');
    let res = await fetch(`http://localhost:4000/cart/saveCart/${userId}`, {
      method: "PUT",
      headers: {'Content-Type': 'application/json'},
      body : JSON.stringify(body)
    });
    if (res.status === 200) {
      const data = await res.json(); 
    }
  }
  const updateQuantity = async (itemId: string, newQuantity: number, sign: boolean) => {
    setCartItems(prevCartItems => {
      const updatedCartItems = prevCartItems.map((item: any) => {
        if (item.product.id === itemId) {
          const updatedQuantity = item.orderedQuantity + (sign ? 1 : -1);

          const orderedQuantity = Math.max(updatedQuantity, 0);
          if(orderedQuantity > 0) 
          console.log(item.product.price)       
            setTotal(total + (sign ? item.product.price : -item.product.price));
          return { ...item, orderedQuantity };
        }
        return item;
      });
  
      saveCart(updatedCartItems); 
  
      return updatedCartItems; 
    });
  };
  
  

  useEffect(() => {
    const userId = localStorage.getItem('user');
    if (userId) {
      fetch(`http://localhost:4000/cart/${userId}`)
        .then((response) => response.json())
        .then((data) => {
          setCartItems(data);
          const totalPrice = data.reduce((accumulator: number, currentItem: any) => {
            return accumulator + (currentItem.product.price * currentItem.orderedQuantity);
          }, 0);
          setTotal(totalPrice);
        })
        .catch((error) => console.error('Erreur lors du chargement des données', error));
    }
  }, []);
  
  return (
    <div className="cart-page">
      <h1>Your Cart</h1>
      <table>
        <thead>
          <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {cartItems && cartItems.length > 0 ? (
            cartItems.map((item: any) => (
              <tr key={item.product.id}>

                <td>
                  <div>
                    <button onClick={()=>{deleteMeal(item.product.id)}}>
                    <FontAwesomeIcon className='delete-icon' icon={faTrash} />
                    </button>
                    {item.product.name}
                  </div>
                </td>
                <td>${Number(item.product.price).toFixed(2)}</td>
                <td>
                  {
                    item.orderedQuantity != 1
                    ? (
                      <button onClick={() => updateQuantity(item.product.id, item.orderedQuantity - 1, false)}>-</button>
                    )
                    : (
                      null
                    )
                  }
                  {item.orderedQuantity}
                  {
                    !(item.orderedQuantity >= item.product.inventory)
                      ? 
                        <button disabled={item.orderedQuantity >= item.product.inventory} onClick={() => updateQuantity(item.product.id, item.orderedQuantity + 1, true )}>+</button>      
                      : <> (Sold Out)</>
                  }
                </td>
                <td>${(Number(item.product.price) * item.orderedQuantity).toFixed(2)}</td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan={4}>Your cart is empty.</td>
            </tr>
          )}
        </tbody>
      </table>
      <div className="cart-total">
        <span>Total : ${Number(total).toFixed(2)}</span>
      </div>
      <button onClick={()=>navigate('/delivery')} className="confirm-button">Confirm</button>
    </div>
  );
};

export default CartComponent;
