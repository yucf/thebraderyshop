import { Product } from "./Product";

export interface Cart {
    _id: string,
    meals: [Product],
    quantities: [number]
}