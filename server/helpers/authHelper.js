const User = require('../models/User');
const Cart= require('../models/Cart');

const jwt = require('jsonwebtoken');

const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '../.env') })

const JWT_SECRET = process.env.JWT_SECRET;
module.exports.login = async (email, password) => {
  try {
    const user = await User.findOne({
      where: { email },
    });

    if (user) {
      const isPasswordCorrect = user.password === password;
      if (isPasswordCorrect) {
        return user;
      } else {
        throw new Error('Incorrect password');
      }
    } else {
      throw new Error('User not found');
    }
  } catch (error) {
    console.log(error);
    throw new Error('Login failed: ' + error.message);
  }
};


module.exports.signup = async (user) => {
    try {
      const createdUser = await User.create(user);
      if (createdUser) {
        return createdUser;
      } else {
        throw new Error('Can\'t sign up');
      }
    } catch (error) {
      console.log(error);
      throw error;
    }
  };

//Create token

module.exports.createToken = (id, email) => {
    try {
        const maxAge = 86400;
        return jwt.sign({ 
                id,
                email 
            }, 
            JWT_SECRET, {
            expiresIn: maxAge
        });
    } catch (error) {
        console.log(error)
    }

};


module.exports.checkToken = (token) =>{
    try {
        var decodedToken;
        jwt.verify(token, JWT_SECRET, (err, decToken) => {
        if (err) {
            return false;
        } else {
            decodedToken = decToken;
        }
        });

        return decodedToken;
    } catch (error) {
        console.log(error)
    }
    
}


module.exports.createUserCart = async (userId) => {
  try {
    const cart = await Cart.create({
      user: userId,
      cartItems: [], 
    });

    return cart;
  } catch (error) {
    console.log(error);
    throw new Error('Failed to create cart: ' + error.message);
  }
};






