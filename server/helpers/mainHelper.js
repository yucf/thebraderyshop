const { default: mongoose } = require('mongoose');
const Product = require('../models/Product')
const Order = require('../models/Order');
const Cart = require('../models/Cart');

module.exports.fetchAllProducts = async () => {
    try {
      const products = await Product.findAll();
      return products;
    } catch (error) {
      throw new Error('failed to fetch products: ' + error.message);
    }
  }

module.exports.orderMeal = async (order) => {
    try {
      const createdOrder = await Order.create(order);
      return createdOrder;
    } catch (error) {
      console.log(error)
      throw new Error('failed to create order: ' + error.message);
    }
  }


module.exports.getCartContent = async (userId) => {
  try {
    const cart = await Cart.findOne({
      where: { user: userId },
    });

    if (cart) {
      const cartItems = cart.cartItems || [];
      const items = await Promise.all
      (
        cartItems.map(async (elet)=>{
        const cart = await Product.findOne({
          where: { id: elet.productId },
        });
        return {product: cart.dataValues, orderedQuantity: elet.orderedQuantity};
      })
      )

      return items.map((item) => ({
        product: {
          ...item.product,
          price: parseFloat(item.product.price),
        },
        orderedQuantity: item.orderedQuantity,
      }));
    } else {
      return [];
    }
  } catch (error) {
    console.log(error)
    throw new Error('Failed to retrieve cart content: ' + error.message);
  }
};




module.exports.updateOrder = async (orderId, updatedFields) =>{
  const updatedOrder = await Order.findByIdAndUpdate(
    orderId,
    { $set: updatedFields },
    { new: true } 
  );
}

module.exports.addToCart = async (userId, productId) => {
  try {
    const [cart, created] = await Cart.findOrCreate({
      where: { user: userId },
    });

    let cartItems = cart.cartItems || [];

    const cartItemIndex = cartItems.findIndex(
      (item) => item.productId === productId
    );

    if (cartItemIndex !== -1) {
      cartItems[cartItemIndex].orderedQuantity += 1;
    } else {
      cartItems.push({
        productId: productId,
        orderedQuantity: 1,
      });
    }

    cart.cartItems = cartItems;

    await cart.changed('cartItems', true);
    await cart.save();

    await Product.decrement('inventory', {
      by: 1,
      where: { id: productId },
    });

    return cart;
  } catch (error) {
    throw new Error('Failed to add product to cart: ' + error.message);
  }
};



module.exports.saveCart = async (userId, cartItems) =>{
  const cart = await Cart.findOne({
    where: { user: userId }
  })
  cart.cartItems = cartItems.map(elet => Object.assign(elet, {productId: elet.product.id}));
  await cart.changed('cartItems', true);
  await cart.save();
  
  return cart;
}
module.exports.clearCart = async (userId) =>{
  const cart = await Cart.findOne({
    where: { user: userId }
  })
  cart.cartItems = [];
  await cart.changed('cartItems', true);
  await cart.save();
  
  return cart;
}




module.exports.deleteFromCart = async (userId, mealId) =>{
  try {
    const userCart = await Cart.findOne({where : { user: userId }});

  if (userCart) {
    const existingMeal = userCart.cartItems.find(meal => String(meal.productId) === String(mealId));

    if (existingMeal && existingMeal.orderedQuantity > 1) {
      existingMeal.orderedQuantity -= 1;
    } else {
      userCart.cartItems = userCart.cartItems.filter(elet => String(elet.productId) !== String(mealId));
    }
    await userCart.changed('cartItems', true);
    await userCart.save();
  }

  await Meal.findOneAndUpdate(
    { _id: mealId },
    { $inc: { quantity: 1 } } 
  );

  return userCart;
  
  } catch (error) {
    console.log(error)
  }
}