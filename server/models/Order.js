const Sequelize = require('sequelize');
const sequelize = require('../db/config.js');

const Order = sequelize.define('Order', {
  user: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: 'users',
      key: 'id',
    },
  },
  cartItems: {
    type: Sequelize.JSONB, 
    defaultValue: [],
  },
  total: {
    type: Sequelize.DECIMAL(10, 2),
    allowNull: false,
  },
  address: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  status: { // PAID, PENDING, DELIVERED,...
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: 'PENDING'
  }
},
{
  timestamps: false, 
});



module.exports = Order;