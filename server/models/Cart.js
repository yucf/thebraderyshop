const Sequelize = require('sequelize');
const sequelize = require('../db/config.js');

const Cart = sequelize.define('Cart', {
    user: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id',
      },
    },
    cartItems: {
      type: Sequelize.JSONB, 
      defaultValue: [],
    },
  }
  , {
    timestamps: false, 
  });
  
  module.exports = Cart;
  
