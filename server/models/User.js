const Sequelize = require('sequelize');
const sequelize = require('../db/config.js');

const User = sequelize.define('User', {
  firstName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  address:{
    type: Sequelize.STRING,
    }
}
, {
  timestamps: false, 
});



module.exports = User;


