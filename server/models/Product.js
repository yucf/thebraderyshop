const Sequelize = require('sequelize');
const sequelize = require('../db/config.js');

const Product = sequelize.define('Product', {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  price: {
    type: Sequelize.DECIMAL(10, 2),
    allowNull: false,
  },
  inventory: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
}, {
  timestamps: false, 
});


module.exports = Product;
