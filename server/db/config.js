const Sequelize = require('sequelize');

const sequelize = new Sequelize('bradery_db', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
});

module.exports = sequelize;
