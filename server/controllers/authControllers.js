
const User = require("../models/User");
const authHelper = require('../helpers/authHelper');


// POST exports
module.exports = {
    signup_post : async (req, res) => {
       const user = req.body;
       authHelper.signup(user)
       .then(async (user)=> {
          await authHelper.createUserCart(user.id);
          const token = authHelper.createToken(user.id, user.username)
          res.status(201).json({user, token});
       })
       .catch((error)=>{
        res.status(204).json({error: error})
       })
      },

    login_post : async (req, res) => {
        const { email, password } = req.body;
        authHelper.login(email, password)
        .then((user)=> {
          const token = authHelper.createToken(user.id, user.email)
          res.status(200).json({user, token});
        }).catch((error)=>{
          res.status(204).json({error})
        })
      
      },

      checkToken : async (req, res) =>{
        const token = req.body.jwt;
        const decodedToken = authHelper.checkToken(token)
        if(decodedToken){
          return res.status(200).json(decodedToken)
        }

        return res.status(403).json({error: 'not auth'})
        
      }
}

